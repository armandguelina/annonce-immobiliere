const { validationResult } = require('express-validator');
const fileHelper = require('../util/file');

const Product = require('../models/product');

exports.getAddProduct = (req, res, next) => {
    res.render('admin/edit-product', {
        pageTitle: 'Add Product',
        path: '/admin/add-product',
        editing: false,
        hasError: false,
        errorMessage: null,
        validationErrors: [],
    });
};

exports.postAddProduct = (req, res, next) => {
    const title = req.body.title;
    // const imageUrl = req.body.imageUrl;
    const image = req.file;
    const price = req.body.price;
    const description = req.body.description;
    const type = req.body.type;
    const publishingStatus = req.body.publishingStatus;
    const goodsStatus = req.body.goodsStatus;
    const date = req.body.date;

    //if not image we throw error like this and return this view
    if (!image) {
        return res.status(422).render('admin/edit-product', {
            pageTitle: 'Add Product',
            path: '/admin/add-product',
            editing: false,
            hasError: true,
            product: {
                title: title,
                price: price,
                description: description,
                type: type,
                publishingStatus: publishingStatus,
                goodsStatus: goodsStatus,
                date: date,
            },
            errorMessage: 'Attached file is not an image!',
            validationErrors: [],
        });
    }

    //collect all error before adding product
    const errors = validationResult(req);

    //if we have some data incorrect
    if (!errors.isEmpty()) {
        console.log(errors.array());
        return res.status(422).render('admin/edit-product', {
            pageTitle: 'Add Product',
            path: '/admin/edit-product',
            editing: false,
            hasError: true,
            product: {
                title: title,
                price: price,
                description: description,
                type: type,
                publishingStatus: publishingStatus,
                goodsStatus: goodsStatus,
                date: date,
            },
            errorMessage: errors.array()[0].msg,
            validationErrors: errors.array(),
        });
    }

    // we set our image url to this image path
    const imageUrl = image.path;

    // now we create product
    const product = new Product({
        title: title,
        price: price,
        description: description,
        imageUrl: imageUrl,
        type: type,
        publishingStatus: publishingStatus,
        goodsStatus: goodsStatus,
        date: date,
        userId: req.user,
    });
    product
        .save()
        .then((result) => {
            console.log('Created Product');
            res.redirect('/admin/products');
        })
        .catch((err) => {
            console.log(err);
        });
};

//for editing product
exports.getEditProduct = (req, res, next) => {
    const editMode = req.query.edit;
    if (!editMode) {
        return res.redirect('/');
    }
    const prodId = req.params.productId;
    Product.findById(prodId)
        .then((product) => {
            if (!product) {
                return res.redirect('/');
            }
            res.render('admin/edit-product', {
                pageTitle: 'Edit Product',
                path: '/admin/edit-product',
                editing: editMode,
                product: product,
                hasError: false,
                errorMessage: null,
                validationErrors: [],
            });
        })
        .catch((err) => console.log(err));
};

exports.postEditProduct = (req, res, next) => {
    const prodId = req.body.productId;
    const updatedTitle = req.body.title;
    const updatedPrice = req.body.price;
    // const updatedImageUrl = req.body.imageUrl;
    const image = req.file;
    const updatedDesc = req.body.description;
    const updatedType = req.body.type;
    const updatedPublishingStatus = req.body.publishingStatus;
    const updatedGoodsStatus = req.body.goodsStatus;
    const updatedDate = req.body.date;

    const errors = validationResult(req);

    //we check product first if we have error
    if (!errors.isEmpty()) {
        return res.status(422).render('admin/edit-product', {
            pageTitle: 'Edit Product',
            path: '/admin/edit-product',
            editing: true,
            hasError: true,
            product: {
                title: updatedTitle,
                price: updatedPrice,
                description: updatedDesc,
                type: updatedType,
                publishingStatus: updatedPublishingStatus,
                goodsStatus: updatedGoodsStatus,
                date: updatedDate,
                _id: prodId,
            },
            errorMessage: errors.array()[0].msg,
            validationErrors: errors.array(),
        });
    }

    Product.findById(prodId)
        .then((product) => {
            if (product.userId.toString() !== req.user._id.toString()) {
                return res.redirect('/');
            }
            product.title = updatedTitle;
            product.price = updatedPrice;
            product.description = updatedDesc;
            product.type = updatedType;
            product.publishingStatus = updatedPublishingStatus;
            product.goodsStatus = updatedGoodsStatus;
            product.date = updatedDate;

            // product.imageUrl = updatedImageUrl;
            //we delete image first in old folder
            if (image) {
                fileHelper.deleteFile(product.imageUrl);
                product.imageUrl = image.path;
            }
            return product.save().then((result) => {
                console.log('UPDATED PRODUCT!');
                res.redirect('/admin/products');
            });
        })
        .catch((err) => console.log(err));
};

exports.getProducts = (req, res, next) => {
    Product.find({ userId: req.user._id })
        // .select('title price -_id')
        // .populate('userId', 'name')
        .then((products) => {
            console.log(products);
            res.render('admin/products', {
                prods: products,
                pageTitle: 'Admin Products',
                path: '/admin/products',
            });
        })
        .catch((err) => console.log(err));
};

exports.postDeleteProduct = (req, res, next) => {
    const prodId = req.body.productId;

    //we try to found product after deleting with deleteOne
    Product.findById(prodId).then((product) => {
        if (!product) {
            return next(new Error('Product not found.'));
        }
        fileHelper.deleteFile(product.imageUrl);
        return Product.deleteOne({ _id: prodId, userId: req.user._id });
    });
    Product.deleteOne({ _id: prodId, userId: req.user._id })
        .then(() => {
            console.log('DESTROYED PRODUCT');
            res.redirect('/admin/products');
        })
        .catch((err) => console.log(err));
};

exports.getCommentProduct = (req, res, next) => {
    Product.findById(req.params.id)
        .then((product) => {
            if (product) {
                console.log('comment Get');
                res.render('../views/comment/comment', {
                    pageTitle: 'comment Product',
                    product: product,
                    user: req.user,
                    path: '/comment-annonce',
                });
            } else console.log('Error to get data : ' + err);
        })
        .catch((err) => console.log(err));
};

exports.postAddCommentProduct = (req, res, next) => {
    console.log('commet patch');

    return Product.findByIdAndUpdate(
        req.params.id,
        {
            $push: {
                comments: {
                    commenterPseudo: req.body.commenterPseudo,
                    text: req.body.text,
                },
            },
        },
        { new: true },
        (err, docs) => {
            if (!err) {
                return res.redirect('/products');
            } else return res.status(400).send(err);
        }
    );
};
