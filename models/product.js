const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const productSchema = new Schema({
    title: {
        type: String,
        required: true,
    },
    price: {
        type: Number,
        required: true,
    },
    description: {
        type: String,
        required: true,
    },
    imageUrl: {
        type: String,
        required: true,
    },
    type: {
        type: String,
        required: true,
        enum: {
            values: ['Vente', 'Location'],
            message: '{VALUE} is not supported\n',
        },
    },
    publishingStatus: {
        type: String,
        required: true,
        enum: {
            values: ['publiée', 'non publiée'],
            message: '{VALUE} is not supported\n',
        },
    },
    goodsStatus: {
        type: String,
        required: true,
        enum: {
            values: ['Disponible', 'Loué', 'Vendu'],
            message: '{VALUE} is not supported\n',
        },
    },
    date: {
        type: Date,
        required: true,
    },
    userId: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
    },
    comments: {
        type: [
            {
                commenterPseudo: String,
                text: String,
            },
        ],
        required: false,
    },
});

module.exports = mongoose.model('Product', productSchema);
