const path = require('path');

const express = require('express');

const commentController = require('../controllers/comment');
const adminController = require('../controllers/admin');

const isAuth = require('../middleware/is-auth');

const router = express.Router();

router.get('/', commentController.getIndex);

router.get('/products', commentController.getProducts);

router.get('/products/:productId', commentController.getProduct);

router.get('/comment-annonce/:id', isAuth, adminController.getCommentProduct);
router.post('/comment/:id', isAuth, adminController.postAddCommentProduct);

module.exports = router;
