const path = require('path');

const express = require('express');
const { body } = require('express-validator');

const adminController = require('../controllers/admin');
const isAuth = require('../middleware/is-auth');
const isAdmin = require('../middleware/is-auth').isAdmin;

const router = express.Router();

// /admin/add-product => GET
router.get('/add-product', isAuth, isAdmin, adminController.getAddProduct);

// /admin/products => GET
router.get('/products', isAuth, isAdmin, adminController.getProducts);

// /admin/add-product => POST
router.post(
    '/add-product',
    [
        body('title').isString().isLength({ min: 5 }).trim(),
        // body('imageUrl').isURL(),
        body('price').isFloat(),
        body('description').isLength({ min: 5, max: 350 }).trim(),
        body('date').isDate().trim(),
    ],
    isAuth,
    isAdmin,
    adminController.postAddProduct
);

router.get(
    '/edit-product/:productId',
    isAuth,
    isAdmin,
    adminController.getEditProduct
);

router.post(
    '/edit-product',
    [
        body('title').isString().isLength({ min: 4 }).trim(),
        // body('imageUrl').isURL(),
        body('price').isFloat(),
        body('description').isLength({ min: 5, max: 350 }).trim(),
        body('date').isDate().trim(),
    ],
    isAuth,
    isAdmin,
    adminController.postEditProduct
);

router.post(
    '/delete-product',
    isAuth,
    isAdmin,
    adminController.postDeleteProduct
);

module.exports = router;
