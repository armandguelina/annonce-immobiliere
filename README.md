# Annonce-immobiliere


# Nodejs-express and ejs
#  Create with npx express-generator --view=ejs annonce-immobiliere

- Année : M2 IWOCS
- Matière: WEB
- TP : n°1

## Auteur(s)

|Nom|Prénom|login|email|
|--|--|--|--|
| *Sow* | *Ibrahima*| *si164644* | *ibrahima.sow1@etu.univ-lehavre.fr* |
| *Guelina* | *Armand*| *gn182528* | *armand.guelina@etu.univ-lehavre.fr* |


## npm install 
## npm start 
## port --> 3000



### A real estate management website contains and displays real estate listings. An offline user must be able to view the ads.