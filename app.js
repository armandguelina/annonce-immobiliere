// var createError = require('http-errors');
// const bcrypt = require('bcryptjs');

var path = require('path');
var express = require('express');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

// const path = require('path');

const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);

const errorController = require('./controllers/error');
const User = require('./models/user');

//security
const csrf = require('csurf');

//error message
const flash = require('connect-flash');
const multer = require('multer');

//multer config
const fileStorage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'images');
    },
    filename: (req, file, cb) => {
        cb(null, new Date().toISOString() + '-' + file.originalname);
    },
});

//extension config for file upload
const fileFilter = (req, file, cb) => {
    if (
        file.mimetype === 'image/png' ||
        file.mimetype === 'image/jpg' ||
        file.mimetype === 'image/jpeg'
    ) {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

var app = express();

//Mongo config
const MONGODB_URI = 'mongodb://localhost:27017/announce';

const store = new MongoDBStore({
    uri: MONGODB_URI,
    collection: 'sessions',
});

const csrfProtection = csrf();

//app.use(errorController.get404);

//Routing config
const adminRoutes = require('./routes/admin');
const commentRoutes = require('./routes/comment');
const authRoutes = require('./routes/auth');

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(
    multer({ storage: fileStorage, fileFilter: fileFilter }).single('image')
);
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//another middleware for file saving
//app.use(express.static(path.join(__dirname, "public")));
app.use('/images', express.static(path.join(__dirname, 'images')));

//  app.use('/', indexRouter);
// app.use('/users', usersRouter);

//session initialize
app.use(
    session({
        secret: 'my secret',
        resave: false,
        saveUninitialized: false,
        store: store,
    })
);

app.use(csrfProtection);
app.use(flash());

app.use((req, res, next) => {
    if (!req.session.user) {
        return next();
    }

    //find user in our session
    User.findById(req.session.user._id)
        .then((user) => {
            if (!user) {
                return next();
            }
            req.user = user;
            console.log('our user is 2102', req.user);
            next();
        })
        .catch((err) => {
            throw new Error(err);
            // next();
        });
});

//security using csrfToken for all request to view isAuthenticated
app.use((req, res, next) => {
    res.locals.isAuthenticated = req.session.isLoggedIn;
    res.locals.csrfToken = req.csrfToken();
    next();
});

app.use((req, res, next) => {
    res.locals.isAuthenticatedandAdmin = req.user;
    // res.locals.csrfToken = req.csrfToken();
    next();
});

app.use('/admin', adminRoutes);
app.use(commentRoutes);
app.use(authRoutes);

//error routes
app.use(errorController.get500);

app.use(errorController.get404);

//mongo connect
mongoose
    .connect(MONGODB_URI)
    .then((result) => {
        console.log('Connected seccessful 2022!');
    })
    .catch((err) => {
        console.log(err);
    });

// admin registration in the database
// const email = 'sow5@gmail.com';
// const password = "Sowhavre";
// User.findOne({ email: email })
//   .then(userDoc => {
//     if (userDoc) {
//       return null;
//     }
//     return bcrypt
//       .hash(password, 12)
//       .then(hashedPassword => {
//         const user = new User({
//           email: email,
//           password: hashedPassword,
//           role: 1,
//           cart: { items: [] }
//         });
//         return user.save();
//       })

//   })
//   .catch(err => {
//     console.log(err);
//   });

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
    // set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get('env') === 'development' ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.render('error');
});

module.exports = app;
