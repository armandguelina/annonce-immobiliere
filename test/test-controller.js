const expect = require('chai').expect;
const sinon = require('sinon');
const mongoose = require('mongoose');

const User = require('../models/user');
const AuthController = require('../controllers/auth');

describe('Auth Controller', function() {
  before(function() {
    mongoose
      .connect(
        'mongodb://localhost:270172/tiraku'
      )
      .then(result => {
        const user = new User({
          email: 'sow12@gmail.com',
          password: 'Sow4havre',
          _id: '956484ff7d46336020i44ee64'
        });
        return user.save();
      })
      .then(() => {
        done();
      });
  });


  it('should throw an error with code 500 if accessing the database fails', function() {
    sinon.stub(User, 'findOne');
    User.findOne.throws();

    const req = {
      body: {
        email: 'sow12@gmail.com',
        password: 'Sow4havre'
      }
    };

  });

  it('should send a response with a valid user status for an existing user', function() {
    const req = { userId: '956484ff7d46336020i44ee64' };
    const res = {
      statusCode: 500,
      userStatus: null,
      status: function(code) {
        this.statusCode = code;
        return this;
      },

    };
  
  });

  after(function() {
      return mongoose.disconnect();
  });
});
