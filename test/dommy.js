const expect = require('chai').expect;

it('should add numbers', function(){
    const a = 2;
    const b = 3;

    expect(a + b).to.equal(5);
})

it('should not give a good result', function(){
    const a = 3;
    const b = 3;

    expect(a + b).not.to.equal(7);
})